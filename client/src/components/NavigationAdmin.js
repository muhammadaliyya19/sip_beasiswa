import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NavigationAdmin extends Component {
    render() {
        return (
            // <div className='navbar'>
            //     <div className="Admin">ADMIN</div>
            //     <Link to='/' className="heading">HOME</Link>
            //     <Link to='/ListBeasiswa'>KELOLA BEASISWA</Link>
            // </div>
            <header id="header" className="d-flex align-items-center">
                <div className="container d-flex align-items-center justify-content-between">
                <h1 className="logo"><a href="/">SIP BEASISWA</a></h1>
                <nav id="navbar" className="navbar">
                    <ul>
                    <li><a className="nav-link scrollto" href="/">HOME</a></li>
                    <li><a className="nav-link scrollto" href="/ListBeasiswa">KELOLA BEASISWA</a></li>                 
                    <li><a className="nav-link scrollto" href="/ListMahasiswa">DATA MAHASISWA</a></li>                 
                    </ul>
                    <i className="bi bi-list mobile-nav-toggle"></i>
                </nav>

                </div>
            </header>
        );
    }
}

export default NavigationAdmin;
import React, { Component } from "react";
import Beasiswa from "../contracts/Beasiswa.json";
import getWeb3 from "../getWeb3";
import emailjs from '@emailjs/browser';
import CKEditor from 'react-ckeditor-component';

import { FormGroup, FormControl, Button } from 'react-bootstrap';

import NavigationAdmin from './NavigationAdmin';
import Navigation from './Navigation';

class AddBeasiswa extends Component {
    constructor(props) {
        super(props)

        this.state = {
            BeasiswaInstance: undefined,
            account: null,
            web3: null,
            judul: '',
            deskrpsi: '',
            main_link: '',
            kuota: ''
        }
    }

    updateJudul = event => {
        this.setState({ judul: event.target.value });
    }

    updateDeskripsi = event => {
        this.setState({ deskrpsi: event.target.value });
    }

    updateMainLink = event => {
        this.setState({ main_link: event.target.value });
    }

    updateKuota = event => {
        console.log(event.target.value);
        this.setState({ kuota: event.target.value });
    }

    sendNotifEmail = event => {
        // event.preventDefault();
        let data_email = {
            judul: this.state.judul,
            deskripsi: this.state.deskrpsi,
            kuota: this.state.kuota,
            from_name: 'SIP BEASISWA',
            to_name:'Sobat SIP Mahasiswa',
            to_email:'muhammadasaadilhaqisya@gmail.com, aliyyailmi20@gmail.com, muhammad1998@student.ub.ac.id',
            reply_to:'muhammadasaadilhaqisya@gmail.com',
            message: 'Pesan baru nih',
        };
        emailjs.send('service_vqcdpvs', 'template_jd5by05', data_email, 'wnFRXLi4rM8nlLcGt');
    }

    // penambahanKandidat = async () => {
    //     await this.state.EvotingInstance.methods.penambahanKandidat(this.state.nama, this.state.partai, this.state.visimisi).send({ from: this.state.account, gas: 1000000 });
    //     // Reload
    //     window.location.reload(false);
    // }

    penambahanBeasiswa = async () => {
        await this.state.BeasiswaInstance.methods.penambahanBeasiswa(this.state.judul, this.state.deskrpsi, this.state.main_link, this.state.kuota).send({ from: this.state.account, gas: 1000000 });
        console.log('OTW send email');
        this.sendNotifEmail();
        console.log('Beres send email');
        window.location.href= '/ListBeasiswa';
    }

    componentDidMount = async () => {

        // FOR REFRESHING PAGE ONLY ONCE -
        if (!window.location.hash) {
            window.location = window.location + '#loaded';
            window.location.reload();
        }
        try {
            // Get network provider and web3 instance.
            const web3 = await getWeb3();

            // Use web3 to get the user's accounts.
            const accounts = await web3.eth.getAccounts();

            // Get the contract instance.
            const networkId = await web3.eth.net.getId();
            const deployedNetwork = Beasiswa.networks[networkId];
            const instance = new web3.eth.Contract(
                Beasiswa.abi,
                deployedNetwork && deployedNetwork.address,
            );
            // Set web3, accounts, and contract to the state, and then proceed with an
            // example of interacting with the contract's methods.

            this.setState({ BeasiswaInstance: instance, web3: web3, account: accounts[0] });

            const owner = await this.state.BeasiswaInstance.methods.Admin().call();
            if (this.state.account === owner) {
                this.setState({ isOwner: true });
            }

        } catch (error) {
            // Catch any errors for any of the above operations.
            alert(
                `Failed to load web3, accounts, or contract. Check console for details.`,
            );
            console.error(error);
        }
    };

    render() {
        if (!this.state.web3) {
            return (
                <div className="CandidateDetails">
                    {this.state.isOwner ? <NavigationAdmin /> : <Navigation />}
                    <div className="CandidateDetails-title">
                        <h1>
                            Loading Web3, accounts, and contract..
                        </h1>
                    </div>
                </div>
            );
        }

        if (!this.state.isOwner) {
            return (
                <div className="CandidateDetails">
                    {this.state.isOwner ? <NavigationAdmin /> : <Navigation />}
                    <div className="CandidateDetails-title">
                        <h1>
                            HANYA ADMIN YANG BISA MENGAKSES
                        </h1>
                    </div>
                </div>
            );
        }
        return (
            <div className="">

                <div className="CandidateDetails">
                {this.state.isOwner ? <NavigationAdmin /> : <Navigation />}
                    <div className="CandidateDetails-title">
                        <h1>
                            Penambahan Beasiswa
                        </h1>
                    </div>
                </div>
                <br></br>
                <div className="row">
                <div className="col-lg-1"></div>
                <div className="col-lg-10">
                    <FormGroup>
                        <label>Masukan Judul</label>
                        <div className="form-input">
                            <FormControl
                                input='text'
                                value={this.state.judul}
                                onChange={this.updateJudul}
                            />
                        </div>
                    </FormGroup>

                    <FormGroup>
                        <label>Main Link</label>
                        <div className="form-input">
                            <FormControl
                                input='text'
                                value={this.state.main_link}
                                onChange={this.updateMainLink}
                            />
                        </div>
                    </FormGroup>

                    <FormGroup>
                        <label>Kuota</label>
                        <div className="form-input">
                            <FormControl
                                input='text'
                                value={this.state.kuota}
                                onChange={this.updateKuota}
                            />
                        </div>
                    </FormGroup>

                    <FormGroup>
                    <label>Deskripsi</label>
                    <div className="form-input">
                            <FormControl
                                input='text'
                                value={this.state.deskrpsi}
                                onChange={this.updateDeskripsi}
                            />
                        </div>
                    {/* <CKEditor 
                        activeClass="editor"
                        content={this.state.deskrpsi} 
                        events={{
                            "change": this.updateDeskripsi
                        }}
                    /> */}
                    
                    </FormGroup>                    
                    <br></br>
                    <Button onClick={this.penambahanBeasiswa} className="btn btn-md btn-primary">
                        Tambah
                    </Button>
                    <br></br>
                    <br></br>
                </div>
                <div className="col-lg-1"></div>                
                </div>

            </div>
        );
    }
}

export default AddBeasiswa;
